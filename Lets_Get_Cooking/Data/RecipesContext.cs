﻿using Lets_Get_Cooking.Models;
using Microsoft.EntityFrameworkCore;
namespace Lets_Get_Cooking.Data
{
    public class RecipesContext : DbContext
    {
        public RecipesContext(DbContextOptions<RecipesContext> options) : base(options)
        {}

        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
    }
}
