﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Lets_Get_Cooking.Migrations
{
    public partial class Database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Recipes",
                columns: table => new
                {
                    rID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    rTitle = table.Column<string>(maxLength: 100, nullable: false),
                    estCookTime = table.Column<int>(nullable: false),
                    rDirections = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recipes", x => x.rID);
                });

            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    iID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    rID = table.Column<int>(nullable: false),
                    iName = table.Column<string>(maxLength: 100, nullable: false),
                    iAmount = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.iID);
                    table.ForeignKey(
                        name: "FK_Ingredients_Recipes_rID",
                        column: x => x.rID,
                        principalTable: "Recipes",
                        principalColumn: "rID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ingredients_rID",
                table: "Ingredients",
                column: "rID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "Recipes");
        }
    }
}
