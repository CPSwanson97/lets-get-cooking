﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lets_Get_Cooking.Models
{
    public class Ingredient
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int iID { get; set; }

        [Required]
        [ForeignKey("Recipe")]
        public int rID { get; set; }

        [Required]
        [MaxLength(100)]
        public string iName { get; set; }

        [Required]
        public string iAmount { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}
