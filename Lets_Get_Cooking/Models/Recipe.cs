﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lets_Get_Cooking.Models
{
    public class Recipe
    {
        public Recipe(){
            //Ingredients = new List<Ingredient>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int rID { get; set; }

        [Required]
        [MaxLength(100)]
        public string rTitle { get; set; }

        [Required]
        public int estCookTime { get; set; }

        [Required]
        public string rDirections { get; set; }

        public virtual ICollection<Ingredient> Ingredients { get; set; }
    }
}
